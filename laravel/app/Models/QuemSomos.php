<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class QuemSomos extends Model
{
    protected $table = 'quem_somos';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 280,
            'height' => 160,
            'path'   => 'assets/img/quem-somos/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 280,
            'height' => 160,
            'path'   => 'assets/img/quem-somos/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 280,
            'height' => 160,
            'path'   => 'assets/img/quem-somos/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 280,
            'height' => 160,
            'path'   => 'assets/img/quem-somos/'
        ]);
    }

}
