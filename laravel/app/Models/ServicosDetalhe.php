<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ServicosDetalhe extends Model
{
    protected $table = 'servicos_detalhes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 425,
            'height' => null,
            'path'   => 'assets/img/servicos/'
        ]);
    }
}
