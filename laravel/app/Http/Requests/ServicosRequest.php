<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo_1' => 'required',
            'texto_1' => 'required',
            'titulo_2' => 'required',
            'texto_2' => 'required',
            'titulo_3' => 'required',
            'texto_3' => 'required',
            'titulo_4' => 'required',
            'texto_4' => 'required',
        ];
    }
}
