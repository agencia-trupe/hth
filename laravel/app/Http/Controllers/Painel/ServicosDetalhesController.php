<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ServicosDetalhesRequest;
use App\Http\Controllers\Controller;

use App\Models\ServicosDetalhe;

class ServicosDetalhesController extends Controller
{
    public function index()
    {
        $registros = ServicosDetalhe::ordenados()->get();

        return view('painel.detalhes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.detalhes.create');
    }

    public function store(ServicosDetalhesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ServicosDetalhe::upload_imagem();

            ServicosDetalhe::create($input);

            return redirect()->route('painel.servicos.detalhes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ServicosDetalhe $registro)
    {
        return view('painel.detalhes.edit', compact('registro'));
    }

    public function update(ServicosDetalhesRequest $request, ServicosDetalhe $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ServicosDetalhe::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.servicos.detalhes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ServicosDetalhe $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.servicos.detalhes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
