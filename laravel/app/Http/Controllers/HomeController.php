<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\QuemSomos;
use App\Models\Servicos;
use App\Models\ServicosDetalhe;
use App\Models\Cliente;
use App\Models\Contato;

class HomeController extends Controller
{
    public function index()
    {
        $banners          = Banner::ordenados()->get();
        $quemSomos        = QuemSomos::first();
        $servicos         = Servicos::first();
        $servicosDetalhes = ServicosDetalhe::ordenados()->get();
        $clientes         = Cliente::ordenados()->get();
        $contato          = Contato::first();

        return view('frontend.home', compact(
            'banners',
            'quemSomos',
            'servicos',
            'servicosDetalhes',
            'clientes',
            'contato'
        ));
    }
}
