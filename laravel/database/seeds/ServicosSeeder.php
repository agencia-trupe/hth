<?php

use Illuminate\Database\Seeder;

class ServicosSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos')->insert([
            'titulo_1' => 'ESTUDO DE<br>GEOMARKETING',
            'texto_1'  => 'Estudo do seu público<br>Análise da concorrência<br>Expansão do seu negócio',
            'titulo_2' => 'DISTRIBUIÇÃO<br>PLANEJADA',
            'texto_2'  => 'Gestão Nacional<br>Mala Direta & Porta a Porta<br>Portal do Cliente',
            'titulo_3' => 'AÇÕES<br>PROMOCIONAIS',
            'texto_3'  => 'Feiras & Eventos<br>Sampling<br>Pesquisa Quali/Quanti',
            'titulo_4' => 'AÇÕES<br>ESPECIAIS',
            'texto_4'  => 'Ações em Pedágios Ecovias<br>Ações nas Estações CPTM<br>Ações no Metrô',
        ]);
    }
}
