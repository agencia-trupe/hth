<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicosTable extends Migration
{
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('titulo_1');
            $table->text('texto_1');
            $table->text('titulo_2');
            $table->text('texto_2');
            $table->text('titulo_3');
            $table->text('texto_3');
            $table->text('titulo_4');
            $table->text('texto_4');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('servicos');
    }
}
