@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Serviços
            <a href="{{ route('painel.servicos.detalhes.index') }}" class="btn btn-warning btn-sm pull-right"><span class="glyphicon glyphicon-th-list" style="margin-right:10px;"></span>Editar Detalhes</a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.servicos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.servicos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
