@include('painel.common.flash')

<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('titulo_1', 'Título 1') !!}
            {!! Form::textarea('titulo_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_1', 'Texto 1') !!}
            {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('titulo_2', 'Título 2') !!}
            {!! Form::textarea('titulo_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2', 'Texto 2') !!}
            {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('titulo_3', 'Título 3') !!}
            {!! Form::textarea('titulo_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_3', 'Texto 3') !!}
            {!! Form::textarea('texto_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('titulo_4', 'Título 4') !!}
            {!! Form::textarea('titulo_4', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_4', 'Texto 4') !!}
            {!! Form::textarea('texto_4', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
