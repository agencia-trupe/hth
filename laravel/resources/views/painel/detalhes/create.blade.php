@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Serviços / Detalhes /</small> Adicionar Detalhe</h2>
    </legend>

    {!! Form::open(['route' => 'painel.servicos.detalhes.store', 'files' => true]) !!}

        @include('painel.detalhes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
