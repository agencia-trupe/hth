@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Serviços / Detalhes /</small> Editar Detalhe</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.servicos.detalhes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.detalhes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
