<div class="servicos" id="servicos">
    <h1 class="title" data-aos="fade-up">SERVIÇOS</h1>

    <div class="servicos-numeros">
        @foreach(range(1, 4) as $i)
        <div class="numero numero-{{ $i }}" data-aos="flip-left" data-aos-delay="{{ 200 + (50 * $i) }}">
            <img src="{{ asset('assets/img/layout/ico'.$i.'.png') }}" alt="">
            <h3>{!! $servicos->{'titulo_'.$i} !!}</h3>
            <span>{{ $i }}</span>
            <p>{!! $servicos->{'texto_'.$i} !!}</p>
        </div>
        @endforeach
    </div>

    <div class="servicos-detalhes">
        @foreach($servicosDetalhes as $index => $detalhe)
        <div class="detalhe {{ ($index + 1) % 2 ? 'detalhe-esq' : 'detalhe-dir' }}">
            <img src="{{ asset('assets/img/servicos/'.$detalhe->imagem) }}" alt="" data-aos="fade-{{ ($index + 1) % 2 ? 'right' : 'left' }}">
            <div class="texto" data-aos="fade-{{ ($index + 1) % 2 ? 'left' : 'right' }}">
                <h3>{{ $detalhe->titulo }}</h3>
                {!! $detalhe->texto !!}
            </div>
        </div>
        @endforeach
    </div>
</div>
