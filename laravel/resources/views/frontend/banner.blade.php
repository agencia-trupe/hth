<div class="home" id="home">
    <div class="banners">
        <div class="triangulo-cima"></div>
        <div class="triangulo-baixo"></div>

        @foreach($banners as $banner)
        <div class="banner" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            <div class="texto">
                @if($banner->titulo)
                <h2>{!! $banner->titulo !!}</h2>
                @endif
                @if($banner->subtitulo)
                <p>{{ $banner->subtitulo }}</p>
                @endif
            </div>
        </div>
        @endforeach
    </div>

    @if($banners->first())
    <div class="texto-banner">
        @if($banners->first()->titulo)
        <h2>{!! $banners->first()->titulo !!}</h2>
        @endif
        @if($banners->first()->subtitulo)
        <p>{{ $banners->first()->subtitulo }}</p>
        @endif
    </div>
    @endif

    <div class="box-marca">
        <img src="{{ asset('assets/img/layout/marca-hth.png') }}" alt="">
    </div>

    <div class="faixa">LOGÍSTICA PUBLICITÁRIA & DISTRIBUIÇÃO PLANEJADA</div>

    <div class="box-portal">
        <div class="bg"></div>
        <div class="conteudo">
            <img src="{{ asset('assets/img/layout/computadores.png') }}" alt="">
            <h4>PORTAL HTH</h4>
            <p>
                _ acompanhamento em tempo real<br>
                _ gerenciamento completo<br>
                _ supervisão geral
            </p>
        </div>
    </div>
</div>
