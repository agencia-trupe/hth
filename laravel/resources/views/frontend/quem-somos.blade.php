<div class="quem-somos" id="quem-somos">
    <div class="texto-principal">
        <h1 class="title" data-aos="fade-right">QUEM<br> SOMOS</h1>
        <div class="texto" data-aos="fade-left">{!! $quemSomos->texto !!}</div>
    </div>

    <div class="imagens">
        @foreach(range(1, 4) as $i)
        <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->{'imagem_'.$i}) }}" alt="" data-aos="flip-left" data-aos-delay="{{ 50 * $i }}">
        @endforeach
    </div>

    <div class="atuacao-nacional">
        <h2 data-aos="fade-right">ATUAÇÃO<br> NACIONAL</h2>
        <div class="texto" data-aos="fade-right" data-aos-delay="200">{!! $quemSomos->atuacao_nacional !!}</div>
        <img src="{{ asset('assets/img/layout/mapa-brasil.png') }}" alt="" data-aos="zoom-in" data-aos-delay="200">
    </div>
</div>
