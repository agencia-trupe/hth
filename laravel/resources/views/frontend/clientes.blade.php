<div class="clientes" id="clientes">
    <h1 class="title" data-aos="fade-up">CLIENTES</h1>

    <div class="clientes-wrapper">
        @foreach($clientes as $cliente)
        <div class="cliente" data-aos="flip-up" data-aos-delay="300">
            <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="">
        </div>
        @endforeach
    </div>
</div>
