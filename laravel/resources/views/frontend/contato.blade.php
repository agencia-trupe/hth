<div class="contato" id="contato">
    <h1 class="title" data-aos="fade-up">CONTATO</h1>

    <div class="contato-wrapper">
        <div class="informacoes" data-aos="fade-right" data-aos-delay="300">
            <img src="{{ asset('assets/img/layout/marca-hth.png') }}" alt="">
            <p>{!! $contato->telefones !!}</p>
            <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
        </div>

        <form action="" method="POST" id="form-contato" data-aos="fade-left" data-aos-delay="300">
            <p>FALE CONOSCO</p>
            <div class="contato-flex">
                <div class="col">
                    <input type="text" name="nome" id="nome" placeholder="nome" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                </div>
                <div class="col">
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                </div>
                <input type="submit" value="ENVIAR">
            </div>
            <div id="form-contato-response"></div>
        </form>
    </div>
</div>
