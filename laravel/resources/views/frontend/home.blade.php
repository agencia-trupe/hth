@extends('frontend.common.template')

@section('content')

    @include('frontend.banner')
    @include('frontend.quem-somos')
    @include('frontend.servicos')
    @include('frontend.clientes')
    @include('frontend.contato')

@endsection
