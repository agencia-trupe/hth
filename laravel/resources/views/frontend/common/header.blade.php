    <header>
        <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>

        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>

    <nav class="mobile-nav">
        @include('frontend.common.nav')
    </nav>

    <nav class="fixed-nav">
        @include('frontend.common.nav')
    </nav>
