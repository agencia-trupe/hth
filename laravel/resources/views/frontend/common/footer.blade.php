    <footer>
        <div class="center">
            <p>
                © {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados.
                <br>
                <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>.
            </p>
        </div>
    </footer>
