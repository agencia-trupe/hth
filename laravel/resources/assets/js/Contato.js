export default function Contato() {
    $('#form-contato').on('submit', function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text('Mensagem enviada com sucesso!').fadeIn('slow');
                $form[0].reset();
            },
            error: function() {
                $response.fadeOut().text('Preencha todos os campos corretamente.').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    });
};
