import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import Contato from './Contato';

AjaxSetup();
MobileToggle();
Contato();

var onScroll = function(event) {
    var scrollPos = $(document).scrollTop();

    $('.fixed-nav a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr('href'));

        var compensacao = currLink.attr('href') == '#quem-somos' ? 240 : 0;

        if (refElement.position().top <= scrollPos + compensacao && refElement.position().top + refElement.height() > scrollPos) {
            $('.fixed-nav a').removeClass('active');
            currLink.addClass('active');
        }
    });
};

$(document).on('scroll', onScroll);

var scrollTo = function(section) {
    $('html, body').animate({
        scrollTop: $(section).offset().top
    }, function() {
        $(document).on('scroll', onScroll);
    });
};

$('.menu-scroll').on('click touchstart', function(event) {
    event.preventDefault();
    $(document).off('scroll');

    if (!$(this).hasClass('active')) $('.menu-scroll').removeClass('active');
    $(this).addClass('active');

    var section = $(this).attr('href');
    scrollTo(section);

    if ($(this).parent().is('.mobile-nav')) {
        $('.mobile-nav').slideToggle();
        $('#mobile-toggle').toggleClass('close');
    }
});

onScroll();

$('.banners').cycle({
    slides: '>.banner'
}).on('cycle-before', function(event, options, outSlide, inSlide) {
    $('.texto-banner').fadeOut(function () {
        $('.texto-banner')
            .html($(inSlide)
            .find('.texto').html())
            .fadeIn('fast');
    });
});

