export default function MobileToggle() {
    var $handle = $('#mobile-toggle'),
        $nav    = $('.mobile-nav');

    $handle.on('click touchstart', function(event) {
        event.preventDefault();
        $nav.slideToggle();
        $handle.toggleClass('close');
    });
};
