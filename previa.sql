-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: 08-Out-2018 às 13:46
-- Versão do servidor: 5.7.22
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hth`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `titulo`, `subtitulo`, `created_at`, `updated_at`) VALUES
(1, 1, '70_20181008134035GA0rXQ0o13.jpg', 'GEOMARKETING E<br />\r\nDISTRIBUI&Ccedil;&Atilde;O<br />\r\nPLANJEDA', 'COM INFORMAÇÕES E IMAGENS ON-LINE', '2018-10-08 13:40:36', '2018-10-08 13:40:36'),
(2, 2, '3_201810081342391ia0SbRpHr.jpg', '', '', '2018-10-08 13:42:39', '2018-10-08 13:42:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `ordem`, `titulo`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, 'HTH', 'marca-hth_20181008134206jnvN5BHjxR.png', '2018-10-08 13:42:06', '2018-10-08 13:42:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, 'Home to Home', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefones`, `created_at`, `updated_at`) VALUES
(1, 'hometohome@grupohth.com.br', '11 2283 1638<br />\r\n11 7732 3051<br />\r\n11 2283 2478', NULL, '2018-10-08 13:42:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2018_10_02_145918_create_clientes_table', 1),
('2018_10_02_151437_create_quem_somos_table', 1),
('2018_10_02_161536_create_banners_table', 1),
('2018_10_02_161757_create_servicos_table', 1),
('2018_10_02_162155_create_servicos_detalhes_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `quem_somos`
--

CREATE TABLE `quem_somos` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `atuacao_nacional` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `quem_somos`
--

INSERT INTO `quem_somos` (`id`, `texto`, `imagem_1`, `imagem_2`, `imagem_3`, `imagem_4`, `atuacao_nacional`, `created_at`, `updated_at`) VALUES
(1, '<h3>Somos uma empresa com mais de 26 anos no mercado de Log&iacute;stica Publicit&aacute;ria, durante este per&iacute;odo expandimos nossos servi&ccedil;os e aperfei&ccedil;oamos nosso modelo de neg&oacute;cio de acordo com o mercado.</h3>\r\n\r\n<p>Oferecemos a mais completa solu&ccedil;&atilde;o em servi&ccedil;os de distribui&ccedil;&atilde;o porta a porta, Mala-direta e a&ccedil;&otilde;es customizadas para ativar sua marca localmente.</p>\r\n\r\n<p>Com know-how em processos e sistemas para atender nossos clientes com agilidade e qualidade.</p>\r\n\r\n<p>Somos o <strong>Grupo Home to Home</strong>, prazer em atend&ecirc;-los!&nbsp;</p>\r\n', '40_20181008134127HAcDYXq4HZ.jpg', '24_20181008134127SicnAeKQzx.jpg', '36_20181008134127WlN5YVrZLY.jpg', '25_20181008134127LLegUBQsbe.jpg', '<p>Entendemos a necessidade de nossos clientes e proporcionamos solu&ccedil;&otilde;es para sua estrat&eacute;gia, como &aacute;rea de cobertura, tiragem ideal para cada a&ccedil;&atilde;o a ser divlgada e modelo de distribui&ccedil;&atilde;o.</p>\r\n\r\n<p>Ofercemos estudos de geomapeamento para identifica&ccedil;&atilde;o das &aacute;reas potenciais de acordo com o perfil do cliente/material.</p>\r\n', NULL, '2018-10-08 13:41:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE `servicos` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo_1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_1` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_3` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_3` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_4` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_4` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `titulo_1`, `texto_1`, `titulo_2`, `texto_2`, `titulo_3`, `texto_3`, `titulo_4`, `texto_4`, `created_at`, `updated_at`) VALUES
(1, 'ESTUDO DE<br>GEOMARKETING', 'Estudo do seu público<br>Análise da concorrência<br>Expansão do seu negócio', 'DISTRIBUIÇÃO<br>PLANEJADA', 'Gestão Nacional<br>Mala Direta & Porta a Porta<br>Portal do Cliente', 'AÇÕES<br>PROMOCIONAIS', 'Feiras & Eventos<br>Sampling<br>Pesquisa Quali/Quanti', 'AÇÕES<br>ESPECIAIS', 'Ações em Pedágios Ecovias<br>Ações nas Estações CPTM<br>Ações no Metrô', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos_detalhes`
--

CREATE TABLE `servicos_detalhes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `servicos_detalhes`
--

INSERT INTO `servicos_detalhes` (`id`, `ordem`, `imagem`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 1, '1_201810081343250uDjht1GiE.jpg', 'ESTUDO DE GEOMARKETING', '<p>Nossa empresa conta com um SIG de gerenciamento que obt&eacute;m dados para identificar oportunidades para o seu neg&oacute;cio em sua &aacute;rea de influ&ecirc;ncia, apresentamos o potencial de consumo por segmento para tomada de decis&atilde;o.</p>\r\n', '2018-10-08 13:43:25', '2018-10-08 13:43:25'),
(2, 2, '1_20181008134338jgpWIfpnhL.jpg', 'DISTRIBUIÇÃO PLANEJADA · REVISTAS E JORNAIS', '<p>Distribui&ccedil;&atilde;o de Folhetos, Folders, Cat&aacute;logos e Revistas. Realizamos a entrega destes exemplares porta a porta, com base em roteiriza&ccedil;&atilde;o digital e Geomapeamento, com colaboradores uniformizados, identificados, l&iacute;deres de equipes em campo, acompanhamento via GPS, relat&oacute;rios p&oacute;s distribui&ccedil;&atilde;o com fotos da a&ccedil;&atilde;o. Tudo isso com acompanhamento em nosso PORTAL.</p>\r\n', '2018-10-08 13:43:38', '2018-10-08 13:43:38'),
(3, 3, '1_20181008134350YGj6WwdO5w.jpg', 'SERVIÇOS DE MALA DIRETA PRÉDIOS RESIDENCIAIS', '<p>Maior agilidade e seguran&ccedil;a<br />\r\nManuseio do material (etiqueta/embalagem)&nbsp;<br />\r\nRoteiriza&ccedil;&atilde;o<br />\r\nDistribui&ccedil;&atilde;o dirigida ao morador<br />\r\nProtocolo de todas as entregas</p>\r\n', '2018-10-08 13:43:50', '2018-10-08 13:43:50'),
(4, 4, '1_20181008134359DC1flQp70u.jpg', 'DISTRIBUIÇÃO PORTA A PORTA', '<p>Realizamos a entrega do seu material promocional (Revistas, jornais, cat&aacute;logos, l&acirc;minas e folders) porta a porta, com base em roteiriza&ccedil;&atilde;o digital e Geomapa, com colaboradores &nbsp;uniformizados, com crach&aacute;, l&iacute;deres de equipes, acompanhamento &nbsp;motorizado por supervisores e via GPS, relat&oacute;rios p&oacute;s distribui&ccedil;&atilde;o com fotos da a&ccedil;&atilde;o.</p>\r\n', '2018-10-08 13:44:00', '2018-10-08 13:44:00'),
(5, 5, '1_20181008134415o9AqVckhm6.jpg', 'AÇÕES ESPECIAIS', '<p>A&ccedil;&otilde;es em esta&ccedil;&otilde;es de Metr&ocirc; e CPTM, entrega de folhetos, cat&aacute;logos, jornais, promotoras para a&ccedil;&otilde;es de sampling, demonstra&ccedil;&atilde;o de produtos, divulga&ccedil;&otilde;es de feiras &amp; eventos.<br />\r\nA sua marca interagindo com seu p&uacute;blico diretamente.</p>\r\n', '2018-10-08 13:44:15', '2018-10-08 13:44:15'),
(6, 6, '1_20181008134425YeaoGi6Kmi.jpg', 'PORTAL DO CLIENTE', '<p>Texto pendente.</p>\r\n', '2018-10-08 13:44:25', '2018-10-08 13:44:25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$g1tYsAZ6DUvCv8H1r7AbvucOe7d9Du3FfkB2xmWVwyOaRuCnOmwby', 'XBrLADgSaJOjq6DbRsabXft8IbkgX3jHS0FnCbCvD3gP04EixAdrdHE187TX', NULL, '2018-10-08 13:38:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quem_somos`
--
ALTER TABLE `quem_somos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicos`
--
ALTER TABLE `servicos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicos_detalhes`
--
ALTER TABLE `servicos_detalhes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quem_somos`
--
ALTER TABLE `quem_somos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `servicos`
--
ALTER TABLE `servicos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `servicos_detalhes`
--
ALTER TABLE `servicos_detalhes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
